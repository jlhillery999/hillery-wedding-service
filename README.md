# Hillery Wedding Service

## Project Description

- The Hillery Wedding Service application allows a wedding planner company's employees to create, edit, and delete wedding and expense information. Additionally, it allows the employees to communicate with one another. This applicaion is completely hosted on Google Cloud Platform.

## Technologies Used

* Node.js - version 14.0
* Google Compute Engine
* GCP App Engine
* PostgreSQL
* GCP Datastore
* GCP Cloud Functions
* GCP Cloud Storage
* Firebase

## Features

* User Authentication Service
* Wedding Planner API
* Messenger Service

## Getting Started
   
* In git bash run `git clone https://gitlab.com/jlhillery999/hillery-wedding-service.git`
* In Windows, open command prompt in the /wedding-frontend directory and run `npm i` to install all dependencies
* In the command prompt, run `npm start`. This will run the frontend locally.

## Usage

* Login to the website hosted on localhost:3000
  ![Login Page](./images/login.PNG)
* On the planner page there are many features to interact with
  ![Planner Page](./images/planner.PNG)
  * To open a the page for a specific wedding entry, double-click the row for that entry.
    ![Wedding Page](./images/weddingPage.PNG)
    C
      ![Expense Page](./images/expensePage.PNG)
        * To update the expense's information, click on the "Update Expense Info" button.
          * This will open a form that will allow you to enter data and upload a .jpg or .png image to submit.
            ![Update Expense](./images/updateExpense.PNG)
        * To delete the expense and go to the expenses page, click the "Delete Expense" button. This will prompt you to confirm your decision.
    * To update the wedding's information, click the "Update Wedding Info" button.
      * This will open a form that will allow you to enter data to update its information
        ![Update Wedding](./images/updateWedding.PNG)
    * To delete the wedding and return to the planner page, click the "Delete Wedding" button. This will prompt you to confirm your decision.
    * Add a new expense to this wedding, click on the "Add New Expense" button in the bottom left corner.
      * This will open a form that will allow you to enter data and upload a .jpg or .png image to submit.
        ![Add New Expense](./images/addExpense.PNG)
  * To add a new wedding to the planner, click on the "Add New Wedding" button in the bottom left corner.
    * This will open a form that will allow you to enter data and submit it to the database.
      ![Add New Wedding](./images/addWedding.PNG)
  * To view the wedding archive (table of weddings that have already passed), click on the the "View Wedding Archive" button.
    ![Wedding Archive](./images/archive.PNG)
    * To open a the page for a specific wedding entry, double-click the row for that entry.
  * To view the table of expenses across all weddings, click the "View All Expenses" button
    ![All Expenses](./images/expenseList.PNG)
      * To open a the page for a specific expense entry, double-click the row for that entry.
  * To open the messenger, click the "Open Messenger" button.
    ![Messenger Page](./images/messenger.PNG)
  * To log out of the webpage and return to the login screen, click the "Logout" Button
    * To switch between the inbox and the outbox, click on the respective button.
    * To create and send a message, click the "Create New Message" button.
      * This will open a form that will allow you to enter a recipient and a note to send to said recipient. To send it, click the "Send" button.
        ![Create and Send Message](./images/sendMessage.PNG)